import { FC } from "react";
import { useCounterStore } from "../../store/counter";

const AboutPage: FC = () => {
  const { value } = useCounterStore();

  return <div>value is {value}</div>;
};

export default AboutPage;
