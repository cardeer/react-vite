import { FC } from "react";
import { useCounterStore } from "../../store/counter";

const HomePage: FC = () => {
  const { value, setValue } = useCounterStore();

  return (
    <>
      <div>value is {value}</div>
      <button onClick={() => setValue(value + 1)}>increase</button>
    </>
  );
};

export default HomePage;
