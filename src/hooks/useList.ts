import { useState } from "react";

export function useList() {
  const [list, setList] = useState<string[]>([]);

  const add = (val: string) => {
    const clone = list.slice();
    clone.push(val);
    setList(clone);
  };

  const remove = (index: number) => {
    const clone = list.slice();
    clone.splice(index, 1);
    setList(clone);
  };

  return { list, add, remove };
}
