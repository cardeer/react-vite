import axios from "axios";

export async function getAllTickets() {
  const result = await axios.get("http://localhost:5000/tickets");
  return result.data;
}

export async function addTicket(data: Record<string, any>) {
  await axios.post("", data);
}
