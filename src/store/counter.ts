import { create } from "zustand";

interface ICounterStore {
  value: number;

  setValue: (val: number) => void;
}

export const useCounterStore = create<ICounterStore>((set) => ({
  value: 0,
  setValue(val) {
    set({
      value: val,
    });
  },
}));
