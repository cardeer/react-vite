import { Link, Route, Routes } from "react-router-dom";
import AboutPage from "./modules/about/About";
import HomePage from "./modules/home/Home";

function App() {
  return (
    <>
      <div className="flex gap-[16px] h-[52px] bg-black text-white justify-center items-center">
        <Link to={"/"}>Home</Link>
        <Link to={"/about"}>About</Link>
      </div>

      <Routes>
        <Route path="/" index element={<HomePage />} />
        <Route path="/about" element={<AboutPage />} />
      </Routes>
    </>
  );
}

export default App;
